#!/bin/bash

# Checks if cgroups v2 is available and running

CGROUP2_FILESYSTEMS=$(grep cgroup2 /proc/filesystems)

if [ ! -z "$CGROUP2_FILESYSTEMS" ]
then
    JSON_OUTPUT="{ \"CGROUPSv2_AVAILABLE\" : true "
    CGROUP2_MOUNTS=$(grep cgroup2 /proc/mounts)
    if [ ! -z "$CGROUP2_MOUNTS" ]
    then
        JSON_OUTPUT+=", \"CGROUPSv2_RUNNING\" : true }"
    else
        JSON_OUTPUT+=", \"CGROUPSv2_RUNNING\" : false }"
    fi
else
    JSON_OUTPUT="{ \"CGROUPSv2_AVAILABLE\" : false , \"CGROUPSv2_RUNNING\" : false }"
fi

echo "$JSON_OUTPUT"
