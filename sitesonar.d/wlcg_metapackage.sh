#!/bin/bash

# Print whether the WLCG metapackage is available

WCLG_METAPACKAGE=$(rpm -q HEP_OSlibs)

echo "{ \"WCLG_METAPACKAGE\" : \"$WCLG_METAPACKAGE\" }"
