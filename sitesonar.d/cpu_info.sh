#!/bin/bash

# Print the CPU information

array=()

content=$(cat /proc/cpuinfo)

JSON_OUTPUT="{ "

while read line; do
    if [[ ! "${array[@]}" =~ "${line}" || "${line}" == physical* || "${line}" == core* || "${line}" == apicid* || "${line}" == 'cpu MHz*' ]] ;
    then
        key=$(echo "${line}" | cut -d ":" -f 1 | xargs | tr " " _)
        val=$(echo "${line}" | cut -d ":" -f 2- | xargs)
        # add integer values
        if [[ $key == "cpu_processor" || $key == "apicid" || $key == "bogomips" || $key == "cache_alignment" || $key == "clflush_size" || $key == "core_id" || $key == "cpu_MHz" || $key == "cpu_cores" || $key == "cpu_family" || $key == "cpuid_level" || $key == "model" || $key == "physical_id" || $key == "siblings" || $key == "stepping" ]] 
        then
            JSON_OUTPUT+="\"CPU_$key\" : $val ,"      
        elif [[ $key == "flags" || $key == "power_management" ]]
        then
            string_array=$(echo $val | sed 's| |\" , \"|g') # convert space delimited string to json string array
            JSON_OUTPUT+="\"CPU_$key\" : [ \"$string_array\" ] ,"
        else
            # check for boolean values
            if [[ $val == "yes" ]]
            then 
                JSON_OUTPUT+="\"CPU_$key\" : true ,"
            elif [[ $val == "no" ]]
            then
                JSON_OUTPUT+="\"CPU_$key\" : false ,"
            else
                JSON_OUTPUT+="\"CPU_$key\" : \"$val\" ,"
            fi
        fi
        array[${#array[@]}]=${line}
    fi
done <<< "$content" 2>&1

processor_count=`grep -c "^processor" <<< "$content"`
JSON_OUTPUT+=" \"CPU_processor_count\" : $processor_count }" # prefixing with CPU_ to unqiuely identify these keys
echo $JSON_OUTPUT

if [ $? -ne 0 ]; then
    exit 255
fi

exit $?
