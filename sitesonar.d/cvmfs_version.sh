#!/bin/bash

# Print the CVMFS version

CVMFS_VERSION=$(attr -qg revision /cvmfs/alice.cern.ch)

if [[ $? == 0 ]]; 
then
    echo "{ \"CVMFS_VERSION\" : $CVMFS_VERSION }"
else 
    exit_code=$?
    echo "{ \"CVMFS_VERSION\" : \"\" }"
    exit $exit_code
fi
