#!/bin/bash

# Print the ulimits

ULIMIT_CURRENT=$(ulimit -a)

JSON_OUTPUT="{"
while IFS= read -r line ; 
    do 
    if [ ! -z "$line" ]; then
        key=$(echo "$line" | cut -d $' ' -f 1-3 | xargs | tr ' ' '_')
        val=$(echo "$line" | rev | cut -d $' ' -f 1 | rev | xargs)
        if [[ $val == "unlimited" ]]
        then
            val=999999999 # Replace unlimited values with large positive integer
        fi
        JSON_OUTPUT+="\"ulimit_current_$key\" : $val,"
    fi
done <<< "${ULIMIT_CURRENT}"

ULIMIT_HARD=$(ulimit -aH)

while IFS= read -r line ; 
    do 
    if [ ! -z "$line" ]; then
        key=$(echo "$line" | cut -d $' ' -f 1-3 | xargs | tr ' ' '_')
        val=$(echo "$line" | rev | cut -d $' ' -f 1 | rev | xargs)
        if [[ $val == "unlimited" ]]
        then
            val=999999999 # Replace unlimited values with large positive integer
        fi
        JSON_OUTPUT+="\"ulimit_hard_$key\" : $val,"
    fi
done <<< "${ULIMIT_HARD}"
filemax=$(cat /proc/sys/fs/file-max)
nr_open=$(cat /proc/sys/fs/nr_open)

JSON_OUTPUT+="\"ulimit_filemax\" : $filemax,"
JSON_OUTPUT+="\"ulimit_nropen\" : $nr_open,"
JSON_OUTPUT+=" }"

echo $JSON_OUTPUT | rev | sed '1 s/,//' | rev # remove trailing comma in json
