#!/bin/bash

# Print whether underlay is enabled

UNDERLAY_ENABLED=$(grep "^enable underlay" /etc/singularity/singularity.conf | cut -d "=" -f 2 | xargs)

echo "{ \"UNDERLAY_ENABLED\" : \"$UNDERLAY_ENABLED\" }"
