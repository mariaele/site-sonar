#!/bin/bash

# Check if it is running taskset

function compute_pinned {
processesmask="$1" availcpus="$2" python3 - <<END

def find_processors(s, ch):
    return [i-2 for i, ltr in enumerate(s) if ltr == ch]


def unique(list1):
    # intilize a null list
    unique_list = []

    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    return unique_list

import os
processesmask = os.environ['processesmask']
availcpus = int(os.environ['availcpus'])

fullmask=''.join([char*int(availcpus) for char in '1'])
fullmask = '0b' + fullmask

if availcpus == 0:
    availcpus = 255

mask = bin(int(processesmask, base=16))
mask_pinned=[]
if mask != fullmask:
	mask_pinned = find_processors(mask, '1')
mask_pinned = len(unique(mask_pinned))
if mask_pinned == availcpus:
    print(0)
else:
    print(mask_pinned)
END
}

TASKSET=$(taskset -p $$ | cut -d':' -f2 | xargs)
availcpus=`grep -c "^processor" /proc/cpuinfo`

pinnedcpu=`compute_pinned $TASKSET $availcpus`

ownpinning=0
if [ $pinnedcpu != 0 ]
then
	ownpinning=1
fi

JSON_OUTPUT="{ \"TASKSET\" : \"$TASKSET\", "
JSON_OUTPUT+="\"PERCENTAGE_PINNED\" : $pinnedcpu ,"
JSON_OUTPUT+="\"USING_PINNING\" : $ownpinning }"

echo "$JSON_OUTPUT"
