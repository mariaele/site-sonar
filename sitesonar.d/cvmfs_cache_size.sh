#!/bin/bash

# Checks size of CVMFS cache

CVMFS_CACHE=$(df -P --block-size=1G /cvmfs/alice.cern.ch | awk 'NR > 1 { print $2 }')

JSON_OUTPUT="{ \"CVMFS_CACHE_SIZE\" : ${CVMFS_CACHE:-0} }"

echo "$JSON_OUTPUT"
