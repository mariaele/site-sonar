#! /bin/bash

OUTPUT="$(lscpu | grep 'NUMA')"
NUMAnodes="$(echo "$OUTPUT" | grep 'node(s)' | awk '{print $NF}')"
JSON_OUTPUT="{ \"NUMA_NODES\" : \"$NUMAnodes\" "
for i in $( eval echo {0..$(($NUMAnodes-1))} )
do
   CPUs="$(echo "$OUTPUT" | grep "node$i" | awk '{print $NF}')"
   JSON_OUTPUT+=", \"CPUs_node$i\" : \"$CPUs\" "
done

JSON_OUTPUT+="}"

echo "$JSON_OUTPUT"
