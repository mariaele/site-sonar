#!/bin/bash

# Print the GCC version

GCC_VERSION=$(gcc --version | head -n 1)

if [[ $? == 0 ]]
then
    echo "{ \"GCC_VERSION\" : \"$GCC_VERSION\" }"
else 
    exit_code=$?
    echo "{ \"GCC_VERSION\" : \"\" }"
    exit $exit_code
fi
